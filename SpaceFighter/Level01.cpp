

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	const int COUNT = 21;


	/*This section is used to determine the x position of the spawning point for every enemy within the stage. A Value
	* of zero means that the enemy will spawn on the left side of the screen, and a 1 means on the right side. As such,
	* the range value of spawning locations is betweeen (0 - 1) and the higher the value, the further right the spawn position
	* is.*/

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	

	/*This section is used to determine the delay time for each defined ship in the level. The value starts at 0, meaning
	*the enemy ship will spawn the moment upon the level loading, or after the delay float runs out if it is defined. 
	*A higher value means a longer delay in spawning the enemy ship. */
	double delays[COUNT] =
	{
		0.1, 0.2, 0.1,					//Enemy group 1
		0.4, 0.5, 0.6,					//Enemy group 2
		0.1, 2.0, 0.9, 0.1, 0.11,		//Enemy group 3
		3.25, 4.25, 0.25, 0.1, 5.0,		//Enemy group 4
		2.5, 2.3, 0.3, 0.5, 0.12		//Enemy group 5
	};

	/*This is used as a global level spawning delay for enemies. This value must run out before the enemy ship individual
	*timers start to run for them to spawn. This also applies to spawning between each group of enemies. The value is how
	*long it takes for the next group of enemies to spawn.*/
	float delay = 3.0; // start delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	Level::LoadContent(pResourceManager);
}

