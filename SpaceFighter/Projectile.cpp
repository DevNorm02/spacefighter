
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

/*This method is used to calculate the logic of projectiles. This includes calculating current position, assigned texture, object size, object speed, and when to
despawn the object*/
void Projectile::Update(const GameTime *pGameTime)
{
	//This if statement is used to check if the projectile object is activated (meaning it is current existing in the gamespace.) The following code will run if this is the case.
	if (IsActive())
	{
		/*This section is used to calculate which direction to have the projectile to start moving, and at what speed. This calculation is translated to actual movement
		to the projectile via a translation*/
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		TranslatePosition(translation);

		/*This section is used to calculate the current position of the object in a vector called 'position', and determine the size of the object via getting the size of
		the object's texture*/
		Vector2 position = GetPosition();
		Vector2 size = s_pTexture->GetSize();

		// Is the projectile off the screen?


		/*As the comment above says, this is used to check if the current position of the projectile is outside the visible bounds of the screen. There is a line of code
		*for each boundary of the screen, and the projectile will be despawned if one of these are triggered.*/
		if (position.Y < -size.Y) Deactivate(); //Top Boundary
		else if (position.X < -size.X) Deactivate(); //Right Boundary
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate(); //Bottom Boundary
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate(); //Left Boundary
	}

	//This GameObject::Update is used to send a update to the projectile information each frame. Information will be updated if any changes occured.
	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}