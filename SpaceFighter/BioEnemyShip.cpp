
#include "BioEnemyShip.h"


/*This section of code is used to determine the physical 'characteristics' of the BioEnemyShip. This includes what speed the BioEnemyShip
*will move at, how many hit points the BioEnemyShip will have, and what the radius of collision around the BioEnemyShip is.*/
BioEnemyShip::BioEnemyShip()
{
	SetSpeed(150); //This is used to set the speed of the BioEnemyShip. The Value translates to speed.
	SetMaxHitPoints(1); //This is used to determine the hitpoints of BioEnemyShip. It also acts as a cap, so that an enemy can't have more than the set hit points.
	SetCollisionRadius(20); /*This is used to determine the BioEnemyShip's collision radius. This is used to trigger the interactions that the BioEnemyShip has when
							*an object entets the radius, such as dying to a projectile, or killing the player via touch.*/
}


void BioEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


/*This void essentially contains the code that writes the visual information to the BioEnemyShip object. It will determine Where to draw
* the ship texture and what spiresheet to use, along with what color to set the enemy to. */
void BioEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}
